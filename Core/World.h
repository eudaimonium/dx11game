#include <pch.h>

using namespace DirectX;
using namespace Microsoft::WRL;

class Game;
class Camera;

class World
{
public:
	World(Game* game);
	~World();
	void Initialize();
	void Update(float deltaTime);
	void Render();

private:
	Game* game;
	Camera* camera;

	BasicEffect* gridEffect;
	CommonStates* gridRenderState;
	PrimitiveBatch<VertexPositionColor>* gridPrimitive;

	ComPtr<ID3D11InputLayout> gridInputLayout;

	int gridDivisions = 48;
};

