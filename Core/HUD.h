#pragma once

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Game;
class HUDDebugEntry;

class HUD
{
public:
	HUD(Game* owner);
	void Initialize();
	void Update(float deltaTime);
	void Render();
	void Dispose();

	void WindowSizeChanged(int width, int height);

	void DrawDebugString(std::string name, std::string value);

private:
	Game* game;

	std::unique_ptr<SpriteFont>		font_courierNew;
	std::unique_ptr<SpriteFont>		font_courierNew12;
	std::unique_ptr<SpriteBatch>	spriteBatch;

	int screenWidth;
	int screenHeight;

	Vector2 currentFrameDebugStringPosition;

	std::vector<HUDDebugEntry*>		debugEntryList;
};

