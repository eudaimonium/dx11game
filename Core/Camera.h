#pragma once

#include <pch.h>

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Game;

class Camera
{
public:
	void Initialize(Game* game);
	void Update(float deltaTime);

	void ApplyToEffect(IEffectMatrices* effect);

	float fieldOfView = 80.f;
	float nearPlane = 0.1f;
	float farPlane = 1000.f;
	
	Matrix matrixWorld;
	Matrix matrixView;
	Matrix matrixProjection;
	Matrix matrixMVP;

private:
	Game* game;
	Vector3 position;
	Vector3 direction;

	void UpdateMouseLook(float deltaTime);
};

