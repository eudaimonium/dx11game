#pragma once
#include <pch.h>

using namespace DirectX;

class Input
{
public:
	Input();

	std::unique_ptr<Keyboard> keyboard;
	std::unique_ptr<Mouse> mouse;

	Keyboard::KeyboardStateTracker keyboardTracker;
	Mouse::ButtonStateTracker mouseButtonTracker;

	void SetWindow(HWND window);
	bool IsKeyDown(Keyboard::Keys key);
	bool IsKeyUp(Keyboard::Keys key);
	bool IsMouseLook();
	void Update();
	void ToggleMouseMode();
};
