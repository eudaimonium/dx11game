#pragma once

class Game;

class Graphics
{

public:
	Graphics(Game* owner);
	void GetDefaultSize(int& width, int& height) const;
	void CreateDevice();

	void CreateOutput(HWND window, int width, int height);
	void Clear();

	void WindowSizeChanged(int width, int height);
	void Present();
	void Dispose();

	Microsoft::WRL::ComPtr<ID3D11Device1>           d3dDevice;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext1>    d3dContext;

	int                                             outputWidth;
	int                                             outputHeight;
private:
	HWND                                            m_window;

	D3D_FEATURE_LEVEL                               m_featureLevel;

	Microsoft::WRL::ComPtr<IDXGISwapChain1>         m_swapChain;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView>  m_renderTargetView;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView>  m_depthStencilView;

	Game* game;
};

