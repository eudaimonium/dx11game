#include <pch.h>

#include "Game.h"
#include "Input.h"
#include "Graphics.h"
#include "Camera.h"

//Debug for HUD
#include "HUD.h"

void Camera::Initialize(Game* owner)
{
	game = owner;

	// Start with one unit up
	position = Vector3(0, 1, 0);

	// Facing forward
	direction = Vector3::Forward;

	matrixWorld = Matrix::Identity;
}

void Camera::Update(float deltaTime)
{
	if (game->input->IsMouseLook())
		UpdateMouseLook(deltaTime);

	matrixView = Matrix::CreateLookAt(position, position + direction, Vector3::Up); 

	float width = static_cast<float>(game->graphics->outputWidth);
	float height = static_cast<float>(game->graphics->outputHeight);

	matrixProjection = Matrix::CreatePerspectiveFieldOfView(
		XMConvertToRadians(fieldOfView), 
		width / height, 
		nearPlane, 
		farPlane);

	matrixMVP = (matrixWorld * matrixView) * matrixProjection;
}

void Camera::UpdateMouseLook(float deltaTime)
{
	Mouse::State mouse = Mouse::Get().GetState();

	float mouseDeltaX = mouse.x * deltaTime;
	float mouseDeltaY = mouse.y * deltaTime;

	game->hud->DrawDebugString("Mouse X:", std::to_string(mouse.x));
	game->hud->DrawDebugString("Mouse Y:", std::to_string(mouse.y));
	/*Matrix rotationMatrix = Matrix::CreateFromYawPitchRoll(mouseDeltaX, mouseDeltaY, 0);
	direction = Vector3::Transform(direction, rotationMatrix);*/
}

// Apply world, view and projection matrices to given effect
void Camera::ApplyToEffect(IEffectMatrices* effect)
{
	effect->SetMatrices(matrixWorld, matrixView, matrixProjection);
}
