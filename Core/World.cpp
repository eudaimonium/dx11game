#include "pch.h"
#include "Camera.h"
#include "World.h"
#include "Game.h"
#include "Graphics.h"

World::World(Game* owner)
{
	game = owner;
	camera = new Camera();

	gridEffect = nullptr;
	gridRenderState = nullptr;
	gridPrimitive = nullptr;
}

World::~World()
{
	delete camera;
	delete gridEffect;
	delete gridRenderState;
	delete gridPrimitive;

	gridInputLayout.Reset();
}

void World::Initialize()
{
	camera->Initialize(game);

	ID3D11Device1* device = game->graphics->d3dDevice.Get();
	ID3D11DeviceContext* context = game->graphics->d3dContext.Get();

	gridEffect = new BasicEffect(device);
	gridRenderState = new CommonStates(device);
	gridPrimitive = new PrimitiveBatch<VertexPositionColor>(context);

	gridEffect->SetVertexColorEnabled(true);

	void const* shaderByteCode;
	size_t byteCodeLength;

	gridEffect->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

	DX::ThrowIfFailed(
		device->CreateInputLayout(VertexPositionColor::InputElements,
			VertexPositionColor::InputElementCount, shaderByteCode, byteCodeLength,
			gridInputLayout.ReleaseAndGetAddressOf()));
}

void World::Update(float deltaTime)
{
	camera->Update(deltaTime);
}

void World::Render()
{
	ID3D11DeviceContext* context = game->graphics->d3dContext.Get();

	context->OMSetBlendState(gridRenderState->Opaque(), nullptr, 0xFFFFFFFF);
	context->OMSetDepthStencilState(gridRenderState->DepthNone(), 0);
	context->RSSetState(gridRenderState->CullNone());

	context->IASetInputLayout(gridInputLayout.Get());

	camera->ApplyToEffect(gridEffect);

	gridEffect->Apply(context);
	
	gridPrimitive->Begin();

	int end = gridDivisions / 2;
	int start = -end;

	for (int i = start; i < end; i++)
	{
		VertexPositionColor v1h(Vector3(i, 0, start), Colors::White);
		VertexPositionColor v2h(Vector3(i, 0, end), Colors::White);

		VertexPositionColor v1v(Vector3(start, 0, i), Colors::White);
		VertexPositionColor v2v(Vector3(end, 0, i), Colors::White);

		gridPrimitive->DrawLine(v1h, v2h);
		gridPrimitive->DrawLine(v1v, v2v);
	}

	gridPrimitive->End();
}
