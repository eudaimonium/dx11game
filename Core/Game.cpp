//
// Game.cpp
//

#include "pch.h"
#include "Game.h"
#include <Core/HUD.h>
#include <Core/Graphics.h>
#include <Core/World.h>	
#include <Core/Input.h>

extern void ExitGame();

using namespace DirectX;
using Microsoft::WRL::ComPtr;

Game::Game() noexcept
{
	input = new Input();
	hud = new HUD(this);
	graphics = new Graphics(this);
	world = new World(this);
}

Game::~Game()
{
	delete input;
	delete hud;
	delete graphics;
	delete world;
}

void Game::Initialize(HWND window, int width, int height)
{
	graphics->CreateDevice();
	graphics->CreateOutput(window, width, height);

	hud->Initialize();
	
	world->Initialize();
}

void Game::SetWindowForInput(HWND window)
{
	input->SetWindow(window);
}

// Executes the basic game loop.
void Game::Tick()
{
    m_timer.Tick([&]()
    {
        Update(m_timer);
    });

    Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
    float elapsedTime = float(timer.GetElapsedSeconds());

	input->Update();

	if (input->IsKeyDown(Keyboard::Keys::Enter))
	{
		input->ToggleMouseMode();
	}

	world->Update(elapsedTime);
	hud->Update(elapsedTime);
}

// Draws the scene.
void Game::Render()
{
    // Don't try to render anything before the first Update.
    if (m_timer.GetFrameCount() == 0)
    {
        return;
    }

	// Clear the screen
	graphics->Clear();

	// TODO render cool stuff here
	world->Render();

	// Render HUD
	hud->Render();

    Present();
}

// Presents the back buffer contents to the screen.
void Game::Present()
{
	graphics->Present();
}

// Message handlers
void Game::OnActivated()
{
    // TODO: Game is becoming active window.
}

void Game::OnDeactivated()
{
    // TODO: Game is becoming background window.
}

void Game::OnSuspending()
{
    // TODO: Game is being power-suspended (or minimized).
}

void Game::OnResuming()
{
    m_timer.ResetElapsedTime();

    // TODO: Game is being power-resumed (or returning from minimize).
}

void Game::OnWindowSizeChanged(int width, int height)
{
	graphics->WindowSizeChanged(width, height);
	hud->WindowSizeChanged(width, height);
}


void Game::OnDeviceLost()
{
    // TODO: Add Direct3D resource cleanup here.
	graphics->Dispose();
	hud->Dispose();

	graphics->CreateDevice();
	hud->Initialize();
}