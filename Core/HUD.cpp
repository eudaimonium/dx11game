#include <pch.h>
#include "Game.h"
#include "Graphics.h"
#include "HUD.h"
#include "Core/UI/HUDDebugEntry.h"

HUD::HUD(Game* owner) : debugEntryList()
{
	screenWidth = 800;
	screenHeight = 600;

	game = owner;
}

void HUD::Initialize()
{
	font_courierNew = std::make_unique<SpriteFont>(game->graphics->d3dDevice.Get(), L"Fonts/CourierNew.spritefont");
	font_courierNew12 = std::make_unique<SpriteFont>(game->graphics->d3dDevice.Get(), L"Fonts/CourierNew12.spritefont");

	spriteBatch = std::unique_ptr<SpriteBatch>(new SpriteBatch(game->graphics->d3dContext.Get()));
}

void HUD::Update(float deltaTime)
{
	currentFrameDebugStringPosition.x = 10;
	currentFrameDebugStringPosition.y = screenHeight - 25;
}

void HUD::Render()
{
	// HUD rendering code. Once per frame.
	spriteBatch->Begin();

	const std::string text = "X:" + std::to_string(screenWidth) + "  Y:" + std::to_string(screenHeight);
	const std::string ms = "ms: " + std::to_string(game->m_timer.GetElapsedSeconds() * 1000);
	const std::string fps = "FPS: " + std::to_string(game->m_timer.GetFramesPerSecond());

	font_courierNew12->DrawString(spriteBatch.get(), text.c_str(), Vector2(10, 10), Colors::White, 0.f, Vector2(0, 0));
	font_courierNew12->DrawString(spriteBatch.get(), ms.c_str(), Vector2(10, 35), Colors::White, 0.f, Vector2(0, 0));
	font_courierNew12->DrawString(spriteBatch.get(), fps.c_str(), Vector2(10, 50), Colors::White, 0.f, Vector2(0, 0));

	for (size_t i = 0; i < debugEntryList.size(); i++)
	{
		font_courierNew12->DrawString(
			spriteBatch.get(),
			debugEntryList.at(i)->debugText.c_str(),
			debugEntryList.at(i)->screenPosition,
			Colors::White, 0.f, Vector2::Zero
		);

		delete debugEntryList.at(i);
	}

	debugEntryList.clear();

	spriteBatch->End();
}

void HUD::Dispose()
{
	font_courierNew.reset();
	font_courierNew12.reset();
	spriteBatch.reset();
}

void HUD::WindowSizeChanged(int width, int height)
{
	screenWidth = std::max(width, 320);
	screenHeight = std::max(height, 200);
}

void HUD::DrawDebugString(std::string name, std::string value)
{
	std::string text = name + value;
	Vector2 position(currentFrameDebugStringPosition);

	currentFrameDebugStringPosition.y -= 20;

	HUDDebugEntry* newEntry = new HUDDebugEntry(text, position);
	debugEntryList.push_back(newEntry);
}
