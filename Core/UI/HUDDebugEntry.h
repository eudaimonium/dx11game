#pragma once
#include <pch.h>

using namespace DirectX;
using namespace DirectX::SimpleMath;

class HUDDebugEntry
{
public:
	HUDDebugEntry(std::string text, Vector2 position);
	std::string		debugText;
	Vector2			screenPosition;
};

