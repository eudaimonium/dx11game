#include "pch.h"
#include "Input.h"

Input::Input()
{
	keyboard = std::make_unique<Keyboard>();
	mouse = std::make_unique<Mouse>();
}

void Input::SetWindow(HWND window)
{
	mouse->SetWindow(window);
}

void Input::Update()
{
	keyboardTracker.Update(keyboard->GetState());
	mouseButtonTracker.Update(mouse->GetState());
}

void Input::ToggleMouseMode()
{
	IsMouseLook() ?
		mouse->SetMode(Mouse::MODE_ABSOLUTE) : mouse->SetMode(Mouse::MODE_RELATIVE);
}

bool Input::IsKeyDown(Keyboard::Keys key)
{
	return keyboardTracker.IsKeyPressed(key);
}

bool Input::IsKeyUp(Keyboard::Keys key)
{
	return keyboardTracker.IsKeyReleased(key);
}

bool Input::IsMouseLook()
{
	return mouse->GetState().positionMode == Mouse::MODE_RELATIVE;
}

