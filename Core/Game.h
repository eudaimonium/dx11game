//
// Game.h
//

#pragma once

#include "StepTimer.h"

class HUD;
class Graphics;
class World;
class Input;

// A basic game implementation that creates a D3D11 device and
// provides a game loop.
class Game
{
public:

    Game() noexcept;
	~Game();

    // Initialization and management
    void Initialize(HWND window, int width, int height);
	void SetWindowForInput(HWND window);
    // Basic game loop
    void Tick();

    // Messages
    void OnActivated();
    void OnDeactivated();
    void OnSuspending();
    void OnResuming();
    void OnWindowSizeChanged(int width, int height);
	void OnDeviceLost();
	
	HUD* hud;
	Graphics* graphics;
	World* world;
	Input* input;

    DX::StepTimer m_timer;	

private:

    void Update(DX::StepTimer const& timer);
    void Render();
    void Present();
};
